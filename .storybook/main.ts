import { workspaceConfig } from '@codelab/ci-cd'
import { merge } from 'webpack-merge'
import { customWebpack } from './webpack.custom'

const { storybook } = workspaceConfig


module.exports = {
  stories: (storybook ?? []).map((pkg: string) =>
    `../packages/${pkg}/**/*story.*`,
  ),
  addons: [
    '@storybook/addon-links',
    '@storybook/addon-actions',
    '@storybook/addon-essentials',
  ],
  webpackFinal: async (config: any) => {

    /**
     * Add custom .babelrc config to existing babel-loader
     */
    config.module.rules[0].use[0].options.plugins.push(...[[
      '@babel/plugin-transform-typescript',
      {
        'allowNamespaces': true,
      },
    ]
    ])

    // console.log(util.inspect(config, false, null, true /* enable colconsleors */))

    return merge(config, customWebpack)
  },
}
