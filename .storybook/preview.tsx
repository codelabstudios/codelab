import 'react-grid-layout/css/styles.css'
import 'react-resizable/css/styles.css'
import 'antd/dist/antd.css'

export const parameters = {
  actions: { argTypesRegex: '^on.*' },
}
