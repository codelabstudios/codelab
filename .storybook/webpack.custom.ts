import { Configuration } from 'webpack'
import path from 'path'
import { workspaceConfig } from '@codelab/ci-cd'

const { storybook } = workspaceConfig

const modules = (storybook ?? []).reduce((acc: Array<string>, pkg: string) => {
    return [...acc, path.resolve(__dirname, '../packages', pkg)]
  }
  , [])

export const customWebpack: Configuration = {
  // devtool: 'source-map',
  // module: {
    // rules: [
    //   {
    //     test: /\.map$/,
    //     enforce: 'pre',
    //     use: ['source-map-loader'],
    //   },
    // {
    //   test: /\.js$/,
    //   enforce: 'pre',
    //   use: ['source-map-loader'],
    // },
    // ],
  // },
  resolve: {
    // extensions: ['.map'],
    modules: modules
  },
}

