/**
 * Gulp has issues calling script in parallel, so we break up into separate
 * scripts
 */
import {
  build,
  depGraph,
  dev,
  doc,
  eslint,
  jest,
  storybook,
  updateTsconfig,
} from '@codelab/ci-cd'

/**
 * Dev
 */
exports.dev = dev
exports.storybook = storybook
exports.jest = jest
exports.doc = doc

/**
 * Lint
 */
exports.eslint = eslint
// exports.prettify = prettify

/**
 * Toolchain
 */
exports.updateTsconfig = updateTsconfig

/**
 * Build
 */
exports.build = build

exports.depGraph = depGraph
/**
 * Prod
 */
// exports.build = build
// exports.prod = prod

/**
 Kubernetes
 */
// exports.kubeCreateSecret = kubeCreateSecret
// exports.kubeDeploy = kubeDeploy
// exports.komposeConvert = komposeConvert
// exports.kubeUpdate = kubeUpdate

/**
 * Terraform
 */
// exports.terraformUpdate = terraformUpdate
