# Frontmatter

Order: Used by Gatsby for sorting

Path: URL path

Level:

- 0: No descendents
- 1: Submenu
- 2: Submenu-item

Parent?: Used with level=2 only, use order for reference

Title: Menu title
