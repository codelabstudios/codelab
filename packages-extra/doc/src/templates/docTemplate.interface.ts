export interface Frontmatter {
  order: number
  path: string
  title: string
}
