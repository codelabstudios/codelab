import { Schema } from 'mongoose'
import { DynamicQuery } from 'src/interface/query'
import { Relationship, VertexType } from 'src/interface/graph'

export type MongooseModel = {
  label: string | null
  fields: Array<FieldSchema> | null
  contents: Array<ContentObject> | null
}

export type MongooseModelPayload = {
  [key: string]: string | Array<string>
  _id: string
  graphcms_id: string
}

export type MongooseData = {
  models: Array<MongooseModel>
}

export interface QueryData {
  query: DynamicQuery
  models: Array<MongooseModel>
}

export const toSchemaType = (nameType: string, ref?: string) => {
  switch (nameType) {
    case Relationship.OneToOne:
    case Relationship.ManyToOne: {
      if (!ref) {
        throw new Error(`Missing ref for ${nameType}`)
      }

      return {
        type: Schema.Types.String,
        ref,
      }
    }
    case Relationship.OneToMany:
    case Relationship.ManyToMany: {
      if (!ref) {
        throw new Error(`Missing ref for ${nameType}`)
      }

      return [
        {
          type: Schema.Types.String,
          ref,
        },
      ]
    }
    case 'String':
      return Schema.Types.String
    case 'ObjectId':
      return Schema.Types.ObjectId
    default:
      return {
        type: Schema.Types.String,
      }
  }
}

/**
 * Also acts as the key for Content key-value pair
 */
export type FieldSchema = {
  key: string
  label: string
  ref?: string
  type: string
} & FieldEdgeID

export type FieldValue = {
  value: string
} & FieldEdgeID &
  ContentEdgeID

type ContentEdgeID = {
  contentEdgeID: string
}

type DataNodeID = {
  dataNodeID: string
}

type ContentNodeID = {
  contentNodeID: string
}

type FieldEdgeID = {
  fieldEdgeID: string
}

export type ID = {
  id: string
}

/**
 * After merging field schema & value, id replaced by fieldEdgeID (used to group them together.
 */
export type FieldObject = FieldSchema & FieldValue

export type ContentObject = {
  __typename: VertexType.Content
} & ContentNodeID & {
    fields: Array<
      FieldEdgeID & {
        [key: string]: string
      }
    >
    values: {
      [key: string]: string
    }
  }
