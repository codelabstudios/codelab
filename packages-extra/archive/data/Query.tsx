import React, { ReactNode } from 'react'

type QueryProps<Data> = {
  children({ data }: { data: Data | undefined }): ReactNode
  data: Data
}

export function Query<Data>(props: QueryProps<Data>) {
  const { children, data } = props

  return <>{children({ data })}</>
}
