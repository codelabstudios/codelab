#!/bin/bash

##
  ## Install ssh-agent if not already installed, it is required by Docker.
  ## (change apt-get to yum if you use an RPM-based image)
  ##
  env which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y ) \

  ##
  ## Run ssh-agent (inside the build environment)
  ##
  eval "$(ssh-agent -s)" \

  ##
  ## Add the SSH key stored in SSH_PRIVATE_KEY variable to the agent store
  ## We're using tr to fix line endings which makes ed25519 keys work
  ## without extra base64 encoding.
  ## https://gitlab.com/gitlab-examples/ssh-private-key/issues/1#note_48526556
  ##
  env echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - \

  ##
  ## Create the SSH directory and give it the right permissions
  ##
  mkdir -p ~/.ssh \
  chmod 700 ~/.ssh \

  ##
  ## Optionally, if you will be using any Git commands, set the user name and
  ## and email.
  ##
  #- git config --global user.email "user@example.com"
  #- git config --global user.name "User name"
