import { projectRootDir } from './config'
import { gulpExecSync } from './exec'
import { workspaceConfig, workspaceInitMessage } from 'src/workspace/config'
import { lernaScope } from 'src/workspace/format'

const {
  dev: devPackages,
  storybook: storybookPackages,
  jest: jestPackages,
} = workspaceConfig

export function startDocker(cb) {
  const cmd = `docker-compose -f ${projectRootDir()}/packages/ci-cd/docker/docker-compose.dev.yml -p codelab-builder up -d`

  return gulpExecSync(cmd, cb)
}

export function dev(cb) {
  console.log(devPackages)
  try {
    workspaceInitMessage('dev', devPackages)
  } catch (e) {
    console.log(e.message)
    return cb()
  }

  const cmd = `lerna run --parallel --loglevel=silent --scope=${lernaScope(
    devPackages,
  )} dev`

  return gulpExecSync(cmd, cb)
}

export function storybook(cb) {
  try {
    workspaceInitMessage('storybook', storybookPackages)
  } catch (e) {
    console.log(e.message)
    return cb()
  }

  const cmd = `start-storybook -p 9000 -c .storybook`

  return gulpExecSync(cmd, cb)
}

export function doc(cb) {
  const cmd = `yarn lerna --scope={@codelab/doc,} run dev`

  return gulpExecSync(cmd, cb)
}

export function jest(cb) {
  try {
    workspaceInitMessage('jest', jestPackages)
  } catch (e) {
    console.log(e.message)
    return cb()
  }

  const cmd = `jest --watch --color --json --outputFile=packages/.jest-test-results.json --verbose`

  return gulpExecSync(cmd, cb)
}
