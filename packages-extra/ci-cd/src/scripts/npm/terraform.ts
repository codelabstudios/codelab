import { gulpExecSync } from './exec'

export function terraformUpdate(cb) {
  const cmd = `cd terraform && terraform init && terraform apply -auto-approve`

  return gulpExecSync(cmd, cb)
}
