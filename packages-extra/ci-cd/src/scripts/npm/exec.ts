import { exec, execSync } from 'child_process'

export function gulpExecSync(cmd, cb) {
  execSync(cmd, {
    stdio: 'inherit',
  })

  return cb()
}

export function gulpExec(cmd, cb) {
  const child = exec(cmd, (err, stdout, stderr) => {
    process.stdout.write(err?.message || '')
    process.stdout.write(stderr)
  })

  // eslint-disable-next-line no-unused-expressions
  child.stdout?.on('data', (data) => {
    process.stdout.write(`${data}`)
  })

  return child
}
