import del from 'del'
import findConfig from 'findup-sync'
import { series } from 'gulp'

require('util').inspect.defaultOptions.depth = null

export const envPath = findConfig('.env.dev')

export function deleteGeneratedFolders(cb) {
  // Glob patterns https://github.com/micromatch/micromatch
  return del(['packages/{api,core}/**/!(node_modules)/__generated__'], cb)
}

export const codegen = series(deleteGeneratedFolders)
