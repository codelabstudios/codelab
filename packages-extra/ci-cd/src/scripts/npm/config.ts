import path from 'path'
import findConfig from 'findup-sync'

export function projectRootDir() {
  const config = findConfig('.env.dev')

  return path.dirname(config ?? '')
}
