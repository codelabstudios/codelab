const packageJson = {
  'docker:build':
    'docker-compose -f config/docker-compose.prod.yml build codelab',
  'docker:up':
    'docker-compose -f config/docker-compose.prod.yml --env-file config/.env.test.docker up --build codelab',
  'docker:volume:ls':
    // eslint-disable-next-line no-template-curly-in-string
    'packages/ci-cd/docker run -it --rm -v ${CACHE_VOL}:/vol busybox ls -l /vol',
  'docker:buildtool':
    'docker-compose -f config/docker-compose.prod.yml up --build build-tool',
  'kubernetes:deploy': 'kubectl apply -f config/deployment.yml',
  'kubernetes:secret':
    'kubectl create secret docker-registry codelab-docker-registry --env-file=.env.dev',
  lint:
    'prettier --config .prettierrc.yml --write "**/*.{js,ts,tsx,yml,yaml,json}"',
  eslint: 'TIMING=1 eslint "packages/utils/**/*.{ts,tsx,js,jsx}" --fix --debug',
  commitlint: 'commitlint --from HEAD~1 --to HEAD --verbose\n',
  scaffold: 'lerna run --scope={@codelab/ci-cd,} scaffold',
  sloc:
    'scc --exclude-dir .git,node_modules,dist,.next,.idea,archive,ci-cd,hygen,eslint-config-code .',
}
