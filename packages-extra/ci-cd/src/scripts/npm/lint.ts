import { argv } from 'yargs'
import { gulpExecSync } from './exec'

export function eslint(cb) {
  const pkg = argv.p || ''

  const cmd = `eslint "packages/${pkg}/**/*.{ts,tsx,js,jsx,md}" --fix`

  return gulpExecSync(cmd, cb)
}

export function prettify(cb) {
  const files = argv.files || ''

  const cmd = `prettier --config .prettierrc.yml --ignore-path .prettierignore --write "**/*.{ts,tsx,json,graphql,md}"`
  // const cmd = `prettier --config .prettierrc.yml --ignore-path .prettierignore --write "${files}"`;

  return gulpExecSync(cmd, cb)
}
