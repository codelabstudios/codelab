import { series } from 'gulp'
import { gulpExecSync } from './exec'

export function buildPackages(cb) {
  console.log(process.cwd())
  const cmd = `lerna run --parallel build`

  return gulpExecSync(cmd, cb)
}

export const build = series(buildPackages)
