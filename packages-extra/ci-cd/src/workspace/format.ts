import { Pkg } from './config'

export const lernaScope = (packages: Array<Pkg> | undefined = []) => {
  return `{${packages.map((pkg) => `@codelab/${pkg},`).join('')}}`
}
