import {
  parseWorkspaceConfig,
  WorkspaceConfig,
  WorkspaceYamlConfig,
} from 'src/workspace/config'

describe('Scripts Config', () => {
  it('maps package names by script', () => {
    const config: WorkspaceYamlConfig = {
      api: ['dev', 'jest'],
      core: ['dev', 'storybook'],
      graph: [],
    }

    const expected: WorkspaceConfig = {
      dev: ['api', 'core'],
      jest: ['api'],
      storybook: ['core'],
    }

    const mapped = parseWorkspaceConfig(config)

    expect(mapped).toMatchObject(expected)
  })
})
