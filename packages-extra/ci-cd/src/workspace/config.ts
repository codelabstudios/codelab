import fs from 'fs'
import os from 'os'
import findup from 'findup-sync'
import yaml from 'js-yaml'
import { mergeWith, reduce } from 'lodash'
import { sprintf } from 'sprintf-js'

/* eslint-disable @typescript-eslint/no-var-requires */
const colors = require('colors/safe')

export const scriptsConfigFile = findup('.scripts.config.yml')

export type Script = 'dev' | 'jest' | 'storybook'
export type Pkg =
  | 'api'
  | 'ci-cd'
  | 'common'
  | 'core'
  | 'core-test'
  | 'd3'
  | 'doc'
  | 'eslint-config-codelab'
  | 'event'
  | 'graph'
  | 'props'
  | 'schema'
  | 'ui'
  | 'utils'

/**
 * The Yaml config separates per package, easier to conceptualize.
 */
export type WorkspaceYamlConfig = {
  [key in Pkg]?: Array<Script>
}

/**
 * This parsed config splits by script type, so each script can easily grab the packages it needs to compile.
 */
export type WorkspaceConfig = {
  [key in Script]?: Array<Pkg>
}

let workspaceYamlConfig: WorkspaceYamlConfig = {}

try {
  workspaceYamlConfig = yaml.safeLoad(
    fs.readFileSync(scriptsConfigFile ?? '', 'utf8'),
  ) as WorkspaceYamlConfig
} catch (e) {
  console.log(e)
}

export const parseWorkspaceConfig = (
  config: WorkspaceYamlConfig,
): WorkspaceConfig =>
  reduce<WorkspaceYamlConfig, WorkspaceConfig>(
    config,
    (acc: WorkspaceConfig, scripts: Array<Script> | undefined, pkg) => {
      if (!scripts) {
        return acc
      }

      // Map script to package
      const scriptsToPkgMap: WorkspaceConfig = reduce(
        scripts,
        (a, script) => {
          return {
            ...a,
            [script]: [pkg],
          }
        },
        {},
      )

      return mergeWith(
        acc,
        scriptsToPkgMap,
        (target: Array<Pkg> = [], src: Array<Pkg> = []) => {
          return [...target, ...src].sort()
        },
      )
    },
    {},
  )

export const workspaceConfig = parseWorkspaceConfig(workspaceYamlConfig)

export const workspaceInitMessage = (
  script: Script,
  workspaces: Array<Pkg> | undefined = [],
) => {
  if (!workspaces?.length) {
    throw new Error(
      sprintf(
        '%s %s %s %s %s %s',
        colors.brightRed('error'),
        colors.brightWhite('Configure'),
        colors.brightGreen('.scripts.config.yml'),
        'to enable',
        colors.brightGreen(script),
        'for at least 1 package',
      ),
    )
  }

  const packages = workspaces
    .map((workspace) =>
      sprintf(
        '    %s %s',
        colors.brightWhite('-'),
        colors.brightGreen(`@codelab/${workspace}`),
      ),
    )
    .join(os.EOL)

  const msg = sprintf(
    '%s %s %s %s%s',
    colors.brightWhite('Starting'),
    colors.brightCyan(script),
    'for...',
    os.EOL,
    packages,
  )

  console.log(msg)
}
