import globby from 'globby'

import madge from 'madge'

const config = {
  // baseDir: 'packages/core',
  tsConfig: 'packages/graph/tsconfig.json',
  excludeRegExp: [],
  includeNpm: true,
}

/**
 * Must be called from root, since all paths are relative to that.
 */
export const depGraph = (cb) => {
  // const files = glob.sync('packages/core/**/*')
  const files = globby.sync('packages/graph/**/!(*.spec.*)', {
    gitignore: true,
  })

  console.log(files)

  const graph = madge(files, config)
    // .then((res) => res.dot())
    .then((res) => res.image('depGraph.svg'))
    .then((output) => {
      console.log(output)
    })
}
