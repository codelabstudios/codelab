/* eslint-disable @typescript-eslint/no-var-requires */
import { activeWorkspaceNames } from 'src/codegen/workspace'

const path = require('path')
const { runner } = require('hygen')
const Logger = require('hygen/lib/logger')
const sh = require('shelljs')

/**
 * Runs hygen generator to add `.eslintrc.yml` to each package
 */
const scaffold = (packageName: string) => {
  console.log(`monorepo-scaffold new --name ${packageName}`)

  runner(`monorepo-scaffold new --name ${packageName}`, {
    templates: path.join(
      __dirname,
      'packages/hygen/templates/monorepo-scaffold',
    ),
    cwd: process.cwd(),
    logger: new Logger(console.log.bind(console)),
    // eslint-disable-next-line global-require
    createPrompter: () => require('enquirer'),
    exec: (action: any, body: any) => {
      const opts = body && body.length > 0 ? { input: body } : {}
      console.log(opts)
      // eslint-disable-next-line global-require
      return require('execa').shell(action, opts)
    },
    debug: !!process.env.DEBUG,
  })
}

// console.log(activeWorkspaceNames)

sh.cd('../hygen')
// console.log(sh.pwd())

activeWorkspaceNames.forEach((packageName: string) => {
  console.log(packageName)
  sh.env.HYGEN_OVERWRITE = 1
  scaffold(packageName)
})
