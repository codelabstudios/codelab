import { updateTsconfig } from 'src/codegen/json'
import { activeWorkspaceNames } from 'src/codegen/workspace'

export { activeWorkspaceNames, updateTsconfig }
