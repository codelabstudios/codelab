/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path')
const findConfig = require('findup-sync')
const glob = require('glob')

// eslint-disable-next-line import/no-dynamic-require
const packageJson = require(findConfig('package.json'))
// const packageJson = require(findConfig('package.json', {
//   cwd: path.resolve(__dirname),
// }))

// Directories only by adding `/` at end
const workspacePattern = `${packageJson.workspaces.packages[0]}`

const normalizedWorkspacePattern = `${path.resolve(
  __dirname,
  `../../../../${workspacePattern}`,
)}/`

const packagesDir = glob.sync(normalizedWorkspacePattern, {
  realpath: true,
})

/**
 * Takes the last directory of a directory path
 *
 * /some/path/here /some/path
 */
const lastDirPath = (dirPath: string) => {
  return dirPath.split(path.sep).pop()
}

export const activeWorkspaceNames = packagesDir.map(lastDirPath)
