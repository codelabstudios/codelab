import path from 'path'
import dot from 'dot-object'
import editJsonFile from 'edit-json-file'
import glob from 'glob'
import { sprintf } from 'sprintf-js'

/* eslint-disable @typescript-eslint/no-var-requires */
const colors = require('colors/safe')

/**
 * Allow us to pass a glob pattern, and an object to merge into existing json file.
 *
 * Good for modifying tsconfig.json across packages.
 */
export const editJson = (files: Array<string>, dotFormat: object) => {
  for (const file of files) {
    const from = path.resolve(__dirname, '../../..')
    const to = file
    const relativeTsconfigPath = path.relative(from, to)

    for (const [key, value] of Object.entries(dotFormat)) {
      const msg = sprintf(
        '%s %s %s',
        'Modifying',
        colors.cyan(relativeTsconfigPath),
        `with ${colors.green(`"${key}": "${value}"`)}`,
      )
      console.log(msg)

      const f = editJsonFile(file)
      f.set(key, value)
      f.save()
    }
  }
}

export const updateTsconfig = (cb) => {
  const data = {
    compilerOptions: {
      rootDir: 'src',
      baseUrl: '.',
      outDir: 'dist',
      tsBuildInfoFile: 'dist/tsconfig.tsbuildinfo',
    },
  }
  const dotFormat = dot.dot(data)
  const workspaceTsconfigs = glob.sync('/*/tsconfig*.json', {
    root: path.resolve(__dirname, '../../..'),
  })

  editJson(workspaceTsconfigs, dotFormat)

  return cb()
}
