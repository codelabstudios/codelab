#
# ---- Base Node ----
FROM node:13.8.0-alpine as base

RUN apk add --update --upgrade --no-cache yarn git

# Set variables
ARG NODE_ENV=''
ENV NODE_ENV "$NODE_ENV"
#RUN export HTTP_PROXY=http://127.0.0.1:3001
#RUN echo "$HTTP_PROXY"

# set working directory
WORKDIR /app

# install and cache app dependencies
COPY . ./

RUN yarn cache dir

# Expose the listening port
EXPOSE 3000

# Run container as non-root (unprivileged) user
# The node user is provided in the Node.js Alpine base image
#USER node

# Run npm start script when container starts
#CMD [ "pm2-runtime", "yarn", "--", "start" ]

CMD if [ "$NODE_ENV" == 'test' ]; then \
  yarn build && yarn test; \
elif [ "$NODE_ENV" == 'prod' ]; then \
  yarn build && yarn prod; \
fi

#ENTRYPOINT ["yarn", "build"]

