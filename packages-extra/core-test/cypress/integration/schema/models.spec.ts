context('/model', () => {
  const xhrData = []

  before(() => {
    cy.server({
      // Here we handle all requests passing through Cypress' server
      onResponse: (response) => {
        if (Cypress.env('RECORD')) {
          const { url, method, body: data } = response
          // We push a new entry into the xhrData array
          xhrData.push({ url, method, data })
        }
      },
    })

    // This tells Cypress to hook into any GET request
    if (Cypress.env('RECORD')) {
      cy.route({
        method: 'GET',
        url: '*',
      })
    }
  })

  after(() => {
    // In record mode, save gathered XHR data to local JSON file
    if (Cypress.env('RECORD')) {
      const path = './cypress/fixtures/fixture.json'
      cy.writeFile(path, xhrData)
    }
  })

  beforeEach(() => {
    cy.visit('/model')
  })

  it('brings up the create form with "Create Model" button', () => {
    const createButton = cy.findButtonByText('Create Model')
    createButton.click()
    cy.get('form').then((subject) => {
      cy.log('Adding model info...')
      const nameField = cy.findByPlaceholderText('Model Name', {
        container: subject,
      })
      nameField.type('Customer')

      cy.log('Adding first field...')
      let addFieldButton = cy.findButtonByText('Add Field', {
        container: subject,
      })
      addFieldButton.click()
      const fieldField = cy.findAllByPlaceholderText('Field Name').first()
      fieldField.type('Name')

      cy.log('Adding second field...')
      addFieldButton = cy.findButtonByText('Add Field', {
        container: subject,
      })
      addFieldButton.click()
      const secondFieldField = cy
        .findAllByPlaceholderText('Field Name')
        .then((secondField) => {
          return secondField[1]
        })
      secondFieldField.type('Email')

      cy.log('Submitting form...')
      const submitButton = cy.findButtonByText('Submit', {
        container: subject,
      })
      submitButton.click()
    })
  })
})
