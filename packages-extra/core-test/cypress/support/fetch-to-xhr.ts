function fetchToXhr() {
  before(() => {
    cy.log('Cypress-Unfetch: Polyfill Fetch >>> XHR Fallback!!')
    // Load the standalone polyfill w/ a closure, prevents race
    let unfetch
    cy.readFile('../../node_modules/unfetch/dist/unfetch.umd.js').then(
      (file) => {
        unfetch = file
        return unfetch
      },
    )

    // Then initialize it before the page loads
    Cypress.on('window:before:load', (win) => {
      // eslint-disable-next-line no-param-reassign
      delete win.fetch
      ;(win as any).eval(unfetch)
      // eslint-disable-next-line no-param-reassign
      win.fetch = (win as any).unfetch
    })
  })
}

fetchToXhr()
