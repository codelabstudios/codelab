const wp = require('@cypress/webpack-preprocessor')
const path = require('path')

const coreTestsPath = path.resolve(__dirname, '../..')

const webpackOptions = {
  resolve: {
    extensions: ['.ts', '.js', '.tsx'],
    modules: [
      // Tests
      path.resolve(coreTestsPath),
      path.resolve(coreTestsPath, './node_modules'),
    ],
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: [/node_modules/],
        use: [
          {
            loader: 'ts-loader',
            options: { transpileOnly: true },
          },
        ],
      },
    ],
  },
}

const options = {
  webpackOptions,
}

module.exports = wp(options)
