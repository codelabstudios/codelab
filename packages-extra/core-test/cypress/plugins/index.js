const express = require('express')
const path = require('path')
const chokidar = require('chokidar')
const waitOn = require('wait-on')

const corePackageFiles = path.resolve(
  __dirname,
  '../../../core/**/!(node_modules|dist)/*.{ts,tsx}',
)
const cypressTypeScriptPreprocessor = require('./cy-ts-preprocessor')

module.exports = (on) => {
  on('file:preprocessor', (file) => {
    /**
     * Watch core changes & re-run
     */
    chokidar.watch(corePackageFiles, {}).on('change', () => {
      waitOn(
        {
          resources: ['http://localhost:3000'],
        },
        (err) => {
          if (err) {
            console.log(err)
            return
          }

          console.log('Rerun!')
          file.emit('rerun')
        },
      )
    })

    return cypressTypeScriptPreprocessor(file)
  })
}
