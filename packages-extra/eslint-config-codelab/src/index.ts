/* eslint-disable @typescript-eslint/no-var-requires */

/**
 * Needs to use require since .eslintrc.yml loads using commonjs
 */
const fs = require('fs')
const path = require('path')
const findConfig = require('findup-sync')
const yaml = require('js-yaml')

let doc

try {
  const file = findConfig('.eslintrc.base.yml', {
    cwd: path.resolve(__dirname),
  })
  doc = yaml.safeLoad(fs.readFileSync(file, 'utf8'))
  // console.log(util.inspect(doc, false, null, true))
} catch (e) {
  console.log(e)
}

module.exports = doc
