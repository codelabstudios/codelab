module.exports = {
  // plugins: ['commitlint-plugin-jira-rules'],
  extends: [
    '@commitlint/config-conventional',
    '@commitlint/config-lerna-scopes',
    // 'jira',
  ],
  parserPreset: {
    parserOpts: {
      issuePrefixes: ['CODE-'],
    },
  },
  rules: {
    'references-empty': [2, 'never'],
    'subject-case': [
      2,
      'never',
      //
      // Remove to allow build(doc): CODE-29 some message
      //
      // ['sentence-case', 'start-case', 'pascal-case', 'upper-case'],
      ['start-case', 'pascal-case', 'upper-case'],
    ],
  },
}
