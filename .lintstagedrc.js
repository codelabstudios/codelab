const { CLIEngine } = require('eslint')
const cli = new CLIEngine({})

module.exports = {
  '**/*.{ts,tsx}': (files) => {
    const processedFiles = files
      .map((file) => `${file}`)
      .filter((file) => !cli.isPathIgnored(file))
      .join(' ')

    console.log(processedFiles)

    // const cmd = `eslint --max-warnings 0 ${processedFiles}`;
    const cmd = `eslint ${processedFiles} --fix`

    console.log(`Running: ${cmd}`)
    return cmd
  },
  // '*.{ts,tsx,json,graphql,md}': files => `gulp prettify --files
  // ${files.filter(file => true)}`,
}
