import { CompositionEventKeys } from 'src/Event.interface'

export const compositionEventHandlerKeys: Array<CompositionEventKeys> = [
  'onCompositionEnd',
  'onCompositionEndCapture',
  'onCompositionStart',
  'onCompositionStartCapture',
  'onCompositionUpdate',
  'onCompositionUpdateCapture',
]
