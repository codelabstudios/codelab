import { ImageEventKeys } from 'src/Event.interface'

export const imageEventHandlerKeys: Array<ImageEventKeys> = [
  'onLoad',
  'onLoadCapture',
  'onError',
  'onErrorCapture',
]
