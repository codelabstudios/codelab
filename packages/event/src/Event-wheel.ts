import { WheelEventKeys } from 'src/Event.interface'

export const wheelEventHandlerKeys: Array<WheelEventKeys> = [
  'onWheel',
  'onWheelCapture',
]
