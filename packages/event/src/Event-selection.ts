import { SelectionEventKeys } from 'src/Event.interface'

export const selectionEventHandlerKeys: Array<SelectionEventKeys> = [
  'onSelect',
  'onSelectCapture',
]
