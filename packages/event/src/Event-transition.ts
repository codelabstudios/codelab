import { TransitionEventKeys } from 'src/Event.interface'

export const transitionEventHandlerKeys: Array<TransitionEventKeys> = [
  'onTransitionEnd',
  'onTransitionEndCapture',
]
