import { UiEventKeys } from 'src/Event.interface'

export const uiEventHandlerKeys: Array<UiEventKeys> = [
  'onScroll',
  'onScrollCapture',
]
