import { FocusEventKeys } from 'src/Event.interface'

export const focusEventHandlerKeys: Array<FocusEventKeys> = [
  'onFocus',
  'onFocusCapture',
  'onBlur',
  'onBlurCapture',
]
