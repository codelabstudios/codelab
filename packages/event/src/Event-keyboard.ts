import { KeyboardEventKeys } from 'src/Event.interface'

export const keyboardEventHandlerKeys: Array<KeyboardEventKeys> = [
  'onKeyDown',
  'onKeyDownCapture',
  'onKeyPress',
  'onKeyPressCapture',
  'onKeyUp',
  'onKeyUpCapture',
]
