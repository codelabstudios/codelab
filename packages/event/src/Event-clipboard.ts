import { ClipboardEventKeys } from 'src/Event.interface'

export const clipboardEventHandlerKeys: Array<ClipboardEventKeys> = [
  'onCopy',
  'onCopyCapture',
  'onCut',
  'onCutCapture',
  'onPaste',
  'onPasteCapture',
]
