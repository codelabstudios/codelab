import { AnimationEventKeys } from 'src/Event.interface'

export const animationEventHandlerKeys: Array<AnimationEventKeys> = [
  'onAnimationStart',
  'onAnimationStartCapture',
  'onAnimationEnd',
  'onAnimationEndCapture',
  'onAnimationIteration',
  'onAnimationIterationCapture',
]
