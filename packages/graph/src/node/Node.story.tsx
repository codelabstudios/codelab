import { D3Graph, D3Tree, D3TreeData } from '@codelab/d3'
import React from 'react'
import { Node } from 'src/node/Node'
import { Mapper } from 'src/node/Node.interface'
import { componentData } from 'src/tree/data/Tree-component.data'
import { makeGraph, makeTree } from 'src/tree/Tree'
import { map } from 'src/tree/Tree-map'

export default {
  // component: Node,
  title: 'Node',
}

export const Tree = () => {
  const nodeTree = makeTree(componentData)
  console.log(nodeTree)

  const treeMapper: Mapper<any, D3TreeData> = (node) => {
    return {
      id: node.id,
      label: node.id,
    }
  }
  const mappedTree = map<any, D3TreeData>('children', 'children')(
    treeMapper,
    nodeTree,
  )

  console.log(mappedTree)

  return <D3Tree data={mappedTree} />
}

export const Graph = () => {
  const nodeGraph = makeGraph(componentData)
  const { nodes, links } = nodeGraph.D3Graph

  console.log(nodeGraph.D3Graph)

  return <D3Graph nodes={nodes} links={links} />
}
