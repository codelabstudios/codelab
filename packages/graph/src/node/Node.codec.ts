/**
 * Recursive types
 *
 * https://www.npmjs.com/package/io-ts/v/1.0.5#recursive-types
 * https://github.com/gcanti/io-ts-codegen/issues/42
 * https://github.com/gcanti/io-ts/issues/307
 */
import { decode } from '@codelab/common'
import { Props } from '@codelab/props'
import * as t from 'io-ts'
import { Context, Validation } from 'io-ts'
import { withFallback } from 'io-ts-types/lib/withFallback'
import { v4 as uuidv4 } from 'uuid'

export interface ReactNodeInput<P extends Props = any> {
  type: string
  id?: string
  props?: P
  children?: Array<ReactNodeInput<P>>
}

const reactNodeInput: t.Type<ReactNodeInput> = t.recursion(
  'ReactNodeInput',
  (self) =>
    t.intersection([
      t.type({
        type: t.string,
      }),
      t.partial({
        // TODO withFallback uuid
        // https://github.com/gcanti/io-ts-types/issues/103
        id: t.string,
        props: withFallback(t.object, {}),
        children: t.array(self),
      }),
    ]),
)

export interface DefaultNodeInput<P extends Props = any> {
  id: string
  props?: P
  children?: Array<DefaultNodeInput<P>>
}

const idNodeInput: t.Type<DefaultNodeInput> = t.recursion(
  'DefaultNodeInput',
  (self) =>
    t.intersection([
      t.type({
        id: t.string,
      }),
      t.partial({
        type: withFallback(t.string, ''),
        props: withFallback(t.object, {}),
        children: t.array(self),
      }),
    ]),
)

/**
 * We want the type below with children overridden
 *
 * ```
 * type NodeInput = Overwrite<ReactNodeInput & IDNodeInput, { children: NodeInput }>
 * ```
 */
export type InputNode<P extends Props = any> =
  | ReactNodeInput<P>
  | DefaultNodeInput<P>

const nodeInput: t.Type<InputNode> = t.union([reactNodeInput, idNodeInput])

/**
 * After initializing data, Node becomes NodeDTO, and optional fields are changed to required, with default values set.
 */
export interface DTONode<P extends Props = any> {
  id: string
  type: string
  props: P
  children: Array<DTONode<P>>
}

// const nodeDTO: t.Type<NodeDTO> = t.recursion('NodeDTO', (self) =>
//   t.type({
//     id: t.string,
//     type: t.string,
//     props: t.any,
//     children: t.array(self),
//   }),
// )

// <A, O, I>
export const nodeC = new t.Type<DTONode, DTONode, InputNode>(
  'node',
  /**
   * We could just use nodeInput.decode() without creating a custom codec, but this gives us a dedicated place to type check our input.
   *
   * In our case, we require either an ID or type.
   */
  // is A
  (u): u is DTONode => {
    console.log(u)

    return false
    // return (
    //   !!(u as NodeDTO).id &&
    //   !!(u as NodeDTO).type &&
    //   !!(u as NodeDTO).props &&
    //   !!(u as NodeDTO).children
    // )
  },
  /**
   * Here we use the nodeInput.decode() to set default params.
   */
  (input: InputNode, context: Context): Validation<DTONode> => {
    const { data } = decode(input, nodeInput)

    // TODO withFallback uuid
    // https://github.com/gcanti/io-ts-types/issues/103
    if (!data.id) {
      data.id = uuidv4()
    }

    return typeof input === 'object'
      ? t.success<DTONode>(data as DTONode)
      : t.failure<DTONode>(input, context)
  },
  t.identity,
)
