import { Props } from '@codelab/props'
import { FunctionComponent, ReactNode } from 'react'
import * as _ from 'ts-toolbelt'
import { Graph } from 'src/graph/Graph.interface'
import { DTONode, InputNode } from 'src/node/Node.codec'

export interface Node<P extends Props> {
  Component: FunctionComponent<any>
  id: string
  readonly key: React.Key
  type: string
  props: Props
  parent?: Node<P>
  children: Array<Node<P>>
  dto: DTONode<P>
  input: InputNode<P>

  addChild(child: InputNode<P> | Node<P>): void

  addParent(parent: Node<P>): void

  hasChildren(): boolean

  readonly renderProps: Props

  Children(rootChildren: ReactNode): ReactNode | Array<ReactNode>
}

export interface HasChildren<T> {
  children?: Array<T>
}

export interface HasParent<SubTree> {
  parent?: SubTree
}

export type Curry<F extends (...args: any) => any> = _.F.Curry<F>

export interface NodeFinderAcc<P extends Props> extends HasParent<any> {
  subTree: Node<P>
  found: Node<P> | null // found node
  id: string // id we want to search for
}

export type NodeIteratee<
  SubTree,
  // SubTree extends HasParent<any>,
  TreeNode extends HasChildren<TreeNode>
> = (acc: SubTree, curr: TreeNode, index: number) => SubTree

export type BuildSubTree<SubTree, TreeNode extends HasChildren<TreeNode>> = (
  nodeAppender: NodeIteratee<SubTree, TreeNode>,
  subTree: SubTree,
  child: TreeNode,
  childIndex: number,
) => SubTree

export interface TreeAcc<P extends Props> extends HasParent<Node<P>> {
  subTree: Node<P>
  prev: Node<P>
  parent: Node<P>
}

export interface GraphAcc<P extends Props> extends HasParent<Node<P>> {
  graph: Graph
  subTree: Node<P>
  parent: Node<P>
}

export const hasChildren = <T extends any>(
  node: HasChildren<T>,
  childrenKey = 'children',
) =>
  typeof node === 'object' &&
  typeof node[childrenKey] !== 'undefined' &&
  node[childrenKey]?.length > 0

export type Mapper<T1, T2 = T1> = (node: T1) => T2

export type CurryReduce<T, R> = (reducerFn: Function, init: R, node: T) => R

export type CurryMap<T1, T2> = (mapFn: Mapper<T1, T2>, node: T1) => T2
