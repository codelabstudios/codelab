import {
  DefaultNodeInput,
  InputNode,
  ReactNodeInput,
} from 'src/node/Node.codec'

/**
 * Must have type
 */
export function isReactNodeInput<P extends object>(
  node: InputNode<P>,
): node is ReactNodeInput<P> {
  return !!(node as ReactNodeInput<P>).type
}

/**
 * Must have ID & no type
 */
export function isIDNodeInput<P extends object>(
  node: InputNode<P>,
): node is DefaultNodeInput<P> {
  return !!node.id && !(node as any).type
}

export function isNodeInput<P extends object>(
  node: InputNode<P>,
): node is InputNode<P> {
  return isReactNodeInput(node) || isIDNodeInput(node)
}
