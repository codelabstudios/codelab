import { decode } from '@codelab/common'
import { evalPropsWithContext, filterRenderProps, Props } from '@codelab/props'
import { reduce } from 'lodash'
import React, { FunctionComponent, ReactNode } from 'react'
import { v4 as uuidv4 } from 'uuid'
import { DTONode, InputNode, nodeC } from 'src/node/Node.codec'
import { isIDNodeInput, isReactNodeInput } from 'src/node/Node.guards'
import { HasChildren, Node as NodeInterface } from 'src/node/Node.interface'

/**
 * Node is instantiated during Tree traversal
 */
export class Node<P extends Props = {}> implements NodeInterface<P> {
  public Component: FunctionComponent<any> = () => null

  public id: string

  public type: string

  // eslint-disable-next-line react/static-property-placement
  public props: Props

  public parent?: Node<P>

  public children: Array<Node<P>> = []

  public dto: DTONode<P>

  public input: InputNode

  /**
   * Can take just ID, but fills out other fields
   */
  constructor(node: InputNode) {
    const { data } = decode(node, nodeC)
    const { props, type, id } = data

    const nodeDTO = data

    this.input = node
    this.dto = nodeDTO
    this.props = props
    this.type = type
    this.id = id
  }

  get key(): React.Key {
    return (this.props.key as React.Key) ?? this.id
  }

  static parseDefaults<P extends Props>(node: any) {
    let props: Props = {}
    let type = ''
    let id: string = uuidv4()
    const children: Array<any> = node.children ? node.children : []

    if (isReactNodeInput<P>(node)) {
      props = node.props ?? ({} as Props)
      type = node.type
    }
    if (isIDNodeInput(node)) {
      id = node.id
    }

    return { props, type, id, children }
  }

  public addChild(child: InputNode<P> | Node<P>) {
    let childNode: Node<P>

    if (child instanceof Node) {
      childNode = child
    } else {
      childNode = new Node<P>(child)
    }

    this.children.push(childNode)
    childNode.addParent(this)
  }

  public addParent(parent: Node<P>) {
    this.parent = parent
  }

  static hasChildren<TreeNode extends HasChildren<TreeNode>>(node: TreeNode) {
    return !!node.children?.length
  }

  /**
   * Check children assigned from json structure
   */
  public hasChildren() {
    return !!this.children.length
  }

  public get renderProps() {
    return evalPropsWithContext(filterRenderProps(this.props)) ?? {}
  }

  /**
   * For current node/component, build a React tree comprising of current parent & all children.
   *
   * Allow recursive building of React tree from the bottom up.
   *
   * Children passed from root tree component
   *
   * const Component = Tree.render(data)
   *
   * ```
   * <Component>{jsxChildren}</Component>
   * ```
   */
  public Children(rootChildren: ReactNode): ReactNode | Array<ReactNode> {
    const children = reduce<Node<P>, Array<ReactNode>>(
      this.children,
      (Components: Array<ReactNode>, child: Node<P>) => {
        const { Component: Child, props, key, children: grandChildren } = child

        console.debug(`${this.type} -> ${child.type}`)

        let ChildComponent: ReactNode = rootChildren ? (
          <Child key={key} {...props} {...this.renderProps}>
            {rootChildren}
          </Child>
        ) : (
          <Child key={key} {...props} {...this.renderProps} />
        )

        if (child.hasChildren()) {
          ChildComponent = (
            <Child key={key} {...props} {...this.renderProps}>
              {child.Children(rootChildren)}
            </Child>
          )
        }

        return [...Components, ChildComponent]
      },
      [],
    )

    return children.length === 1 ? children[0] : children
  }
}
