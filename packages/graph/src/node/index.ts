import { InputNode } from './Node.codec'
import { Node } from 'src/node/Node'

export { InputNode, Node }
