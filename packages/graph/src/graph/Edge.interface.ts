import { Vertex } from 'src/graph/Vertex.interface'

export interface Edge {
  id: string
  start: Vertex
  end: Vertex
}
