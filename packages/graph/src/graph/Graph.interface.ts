import { D3GraphProps } from '@codelab/d3'
import { HasID } from 'src/common.interface'
import { Edge } from 'src/graph/Edge.interface'
import { Vertex } from 'src/graph/Vertex.interface'

export type GraphProps = {
  vertices: Array<Vertex>
  edges: Array<Edge>
}

export interface Graph {
  vertices: Array<Vertex>
  edges: Array<Edge>
  parent?: HasID

  addVertexFromNode(node: HasID): void

  addEdgeFromNodes(start: HasID | undefined | null, end: HasID): void

  readonly D3Graph: D3GraphProps
}
