/**
 * These callbacks are executed when visiting each Node during Tree traversal
 */
import { Props } from '@codelab/props'
import { reduce } from 'lodash'
import { Node } from 'src/node/Node'
import { InputNode } from 'src/node/Node.codec'
import { GraphAcc, NodeFinderAcc, TreeAcc } from 'src/node/Node.interface'
import { treeWalker } from 'src/traversal/Traversal'

export function nodeFinderIteratee<P extends Props = {}>(
  { id, found, subTree }: NodeFinderAcc<P>,
  child: Node<P>,
) {
  if (!subTree || !id) {
    return null
  }

  if (child.id === id) {
    // eslint-disable-next-line no-param-reassign
    found = child
  }

  return {
    id,
    found,
    subTree,
  }
}

export function findNode<P extends Props = {}>(
  id: string | undefined,
  subTree: Node<P>,
): Node<P> | null {
  if (!id) {
    throw new Error(`id is undefined`)
  }

  if (subTree.id === id) {
    return subTree
  }

  return reduce<Node<P> | null, NodeFinderAcc<P>>(
    subTree?.children || [],
    treeWalker<NodeFinderAcc<P>, Node<P>>()(nodeFinderIteratee),
    {
      subTree,
      found: null,
      id,
    },
  ).found
}

export function treeAppenderIteratee<P extends Props>(
  { subTree, parent }: TreeAcc<P>,
  child: InputNode<P>,
) {
  const childNode = new Node<P>(child)
  const parentNode = findNode(parent?.id, subTree)

  if (!parentNode) {
    throw Error(`Node of id ${parent?.id} not found`)
  }

  parentNode.addChild(childNode)

  return {
    prev: childNode,
    subTree,
  }
}

export function graphAppenderIteratee<P extends Props>(
  { graph, subTree, parent }: GraphAcc<P>,
  child: InputNode<P>,
) {
  const node = new Node<P>(child)
  const parentNode = findNode(parent?.id, subTree)

  graph.addVertexFromNode(node)
  graph.addEdgeFromNodes(parentNode, node)

  return {
    graph,
    subTree,
    parent,
  }
}
