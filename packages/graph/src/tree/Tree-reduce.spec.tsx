import { Node } from 'src/node/Node'
import {
  reducerData,
  reducerDataCustomChildrenKey,
} from 'src/tree/data/Tree-reducer.data'
import { reduce } from 'src/tree/Tree-reduce'

describe('Tree reducers', () => {
  type PageStats = {
    views: number
  }

  const reducer = (total: number, node: Node<PageStats>) => {
    return total + (node.props?.views ?? 0)
  }

  it('it reduces each node', () => {
    const viewCount = reduce()(reducer, 0, reducerData)

    expect(viewCount).toEqual(13)
  })

  it('it reduces each node using custom children key', () => {
    const viewCount = reduce('myChildren')(
      reducer,
      0,
      reducerDataCustomChildrenKey,
    )

    expect(viewCount).toEqual(13)
  })
})
