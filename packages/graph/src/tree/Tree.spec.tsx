import { InputNode } from 'src/node/Node.codec'
import { makeTree } from 'src/tree/Tree'

describe('Tree', () => {
  it('returns the root node with properties set', () => {
    const data: InputNode = {
      id: 'A',
      children: [
        {
          id: 'B',
        },
        { id: 'C' },
      ],
    }
    const node = makeTree(data)

    expect(node.id).toBe('A')
    expect(node.props).toStrictEqual({})
    expect(node.type).toBe('')
  })

  it('sets unique id by default', () => {
    const data: InputNode = {
      type: 'A',
      children: [
        {
          type: 'B',
        },
        { type: 'C' },
      ],
    }
    const node = makeTree(data)
  })
})
