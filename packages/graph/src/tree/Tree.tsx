/**
 * We construct a tree by traversing the tree data using the treeAppender strategy
 */
import { Props } from '@codelab/props'
import { reduce } from 'lodash'
import { Graph } from 'src/graph/Graph'
import { Node } from 'src/node/Node'
import { DTONode, InputNode } from 'src/node/Node.codec'
import { GraphAcc, TreeAcc } from 'src/node/Node.interface'
import { treeWalker } from 'src/traversal/Traversal'
import {
  graphAppenderIteratee,
  treeAppenderIteratee,
} from 'src/traversal/Traversal-iteratee'

/**
 * This method generates a non-binary tree given JSON input. Each input node is
 *
 * @param input - Input data with a tree-like structure, in JSON format.
 *
 * @returns Root `Node` of the `Tree`
 *
 * ```typescript
 * const tree = makeTree(data)
 * ```
 *
 */
export function makeTree<P extends Props>(input: InputNode<P>): Node<P> {
  const root = new Node<P>(input)
  const subTreeAcc = {
    subTree: root,
    prev: root,
    parent: root,
  }

  return reduce<DTONode<P>, TreeAcc<P>>(
    root.dto.children,
    treeWalker<TreeAcc<P>, DTONode<P>>(root.dto)(treeAppenderIteratee),
    subTreeAcc,
  ).subTree
}

/**
 * Using Vertex/Edge representation
 */
export function makeGraph<P extends Props>(input: InputNode<P>): Graph {
  // Convert input to Node input structure first, nodeFinder requires Node representation
  const root = makeTree(input)
  const g = new Graph({ vertices: [], edges: [] })
  const subTreeAcc = {
    graph: g,
    subTree: root,
    parent: root,
  }

  g.addVertexFromNode(root)

  return reduce<DTONode<P>, GraphAcc<P>>(
    root.dto.children ?? [],
    treeWalker<GraphAcc<P>, DTONode<P>>(root.dto)(graphAppenderIteratee),
    subTreeAcc,
  ).graph
}
