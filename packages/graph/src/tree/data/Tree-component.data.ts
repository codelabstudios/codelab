import { DefaultNodeInput } from 'src/node/Node.codec'

export const componentData: DefaultNodeInput = {
  id: 'root',
  children: [
    {
      id: 'A',
      children: [
        {
          id: 'B',
          children: [
            {
              id: 'C',
            },
            {
              id: 'D',
            },
          ],
        },
      ],
    },
    {
      id: 'E',
      children: [
        {
          id: 'F',
        },
        {
          id: 'G',
        },
        {
          id: 'H',
        },
      ],
    },
  ],
}
