import v from 'voca'
import { Mapper } from 'src/node/Node.interface'
import {
  mapData,
  mapDataCustomChildrenKey,
  mapDataLowerProps,
} from 'src/tree/data/Tree-map.data'
import { map } from 'src/tree/Tree-map'

describe('Tree mappers', () => {
  type TreeItem = {
    name: string
  }
  const mapper: Mapper<any, TreeItem> = (node) => ({
    ...node,
    props: {
      name: v.decapitalize(node.props?.name),
    },
  })

  it('it maps each node', () => {
    const mappedTreeData = map()(mapper, mapData)

    expect(mappedTreeData).toStrictEqual(mapDataLowerProps)
  })

  it('it maps each node including children', () => {
    const mappedTreeData = map('children', 'myChildren')(mapper, mapData)

    expect(mappedTreeData).toStrictEqual(mapDataCustomChildrenKey)
  })
})
