import * as Joi from '@hapi/joi'
import { Module } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { APP_FILTER } from '@nestjs/core'
import { ControllerModule } from './controller/controller.module'
import { ModelModule } from './model/model.module'
import { ViewModule } from './view/view.module'
import { AllExceptionsFilter } from 'src/allException.filter'
import { AppController } from 'src/app.controller'

// eslint-disable-next-line @typescript-eslint/no-var-requires
const findConfig = require('findup-sync')

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      // load: [configuration],
      envFilePath: findConfig('.env.dev') ?? '',
      validationSchema: Joi.object({
        MONGO_ENDPOINT: Joi.string().required(),
        MONGO_ENDPOINT_DEMO: Joi.string().required(),
      }),
    }),
    ModelModule,
    ViewModule,
    ControllerModule,
  ],
  controllers: [AppController],
  providers: [
    ConfigService,
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
  ],
  exports: [ConfigService],
})
export class AppModule {}
