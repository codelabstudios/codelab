import { Controller, Get, Inject } from '@nestjs/common'
import { Action } from 'src/controller/action/action'
import { ACTION_PROVIDER } from 'src/controller/action/action.providers'

@Controller('controller')
export class ControllerController {
  constructor(@Inject(ACTION_PROVIDER) private readonly action: Action) {}

  @Get('')
  index() {
    return this.action.all()
  }
}
