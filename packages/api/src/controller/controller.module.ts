import { Module } from '@nestjs/common'
import { ActionModule } from './action/action.module'
import { ControllerController } from './controller.controller'

@Module({
  imports: [ActionModule],
  controllers: [ControllerController],
})
export class ControllerModule {}
