import { Module } from '@nestjs/common'
import { actionProviders } from 'src/controller/action/action.providers'
import { EndpointModule } from 'src/controller/action/endpoint/endpoint.module'

@Module({
  imports: [EndpointModule],
  providers: [...actionProviders],
  exports: [...actionProviders],
})
export class ActionModule {}
