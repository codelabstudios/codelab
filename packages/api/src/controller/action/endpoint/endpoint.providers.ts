import {
  Endpoint,
  EndpointConfig,
} from 'src/controller/action/endpoint/endpoint'

export const ENDPOINT_PROVIDER = 'ENDPOINT_PROVIDER'

export const endpointProviders = [
  {
    provide: ENDPOINT_PROVIDER,
    useFactory: (): Endpoint => {
      const config: EndpointConfig = [
        {
          name: 'CREATE_MODEL',
          config: {
            url: 'schema/model',
            method: 'POST',
          },
        },
        {
          name: 'GET_MODEL',
          config: {
            url: 'schema/model',
            method: 'POST',
          },
        },
      ]
      return new Endpoint(config)
    },
  },
]
