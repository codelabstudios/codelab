import { Action, ActionConfig } from 'src/controller/action/action'

export const ACTION_PROVIDER = 'ACTION_PROVIDER'

export const actionProviders = [
  {
    provide: ACTION_PROVIDER,
    useFactory: (): Action => {
      const config: ActionConfig = [
        {
          name: 'onFinish',
          endpoint: 'CREATE_MODEL',
        },
      ]
      return new Action(config)
    },
  },
]
