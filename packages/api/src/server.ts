import { NestFactory } from '@nestjs/core'
import methodOverride from 'method-override'
import bodyParser from 'body-parser'
import { AppModule } from 'src/app.module'
import { router } from 'src/router'

export const bootstrap = async () => {
  const app = await NestFactory.create(AppModule)
  // , {
  //   cors: {
  //     origin: '*',
  //     allowedHeaders: 'Access-Control-Allow-Origin',
  //     methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  //     preflightContinue: false,
  //     optionsSuccessStatus: 204,
  //     credentials: true,
  //   },
  // })
  // TODO make reproduction
  app.enableCors()

  app.use(bodyParser.json())
  app.use(methodOverride())
  app.use(router)

  await app.listen(4000)

  if (module.hot) {
    module.hot.accept()
    module.hot.dispose(() => app.close())
  }
}
