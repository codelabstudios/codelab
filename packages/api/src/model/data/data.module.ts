import { Module } from '@nestjs/common'
import { dataProviders } from 'src/model/data/data.providers'

@Module({
  providers: [...dataProviders],
  exports: [...dataProviders],
})
export class DataModule {}
