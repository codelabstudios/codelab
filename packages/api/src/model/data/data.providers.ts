import { Data } from 'src/model/data/data'

export const DATA_PROVIDER = 'DATA_PROVIDER'

export const dataProviders = [
  {
    provide: DATA_PROVIDER,
    useFactory: (): Data => {
      return new Data()
    },
  },
]
