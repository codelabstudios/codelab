import { Module } from '@nestjs/common'
import { ModelController } from './model.controller'
import { Data } from 'src/model/data/data'
import { DataModule } from 'src/model/data/data.module'
import { SchemaModule } from 'src/model/schema/schema.module'

@Module({
  imports: [SchemaModule, DataModule],
  exports: [SchemaModule],
  providers: [Data],
  controllers: [ModelController],
})
export class ModelModule {}
