import * as mongoose from 'mongoose'

export const FieldSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true,
  },
})

export const ModelSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true,
  },
  fields: [FieldSchema],
})
