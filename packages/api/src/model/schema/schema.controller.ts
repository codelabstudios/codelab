import {
  Body,
  Controller,
  Get,
  HttpService,
  Post,
  UseInterceptors,
} from '@nestjs/common'
import { Observable } from 'rxjs'
import { SchemaInterceptor } from 'src/model/schema/schema.interceptor'

@UseInterceptors(SchemaInterceptor)
@Controller('schema')
export class SchemaController {
  constructor(private httpService: HttpService) {}

  @Get('model')
  get() {
    return this.httpService.get('api/v1/Model')
  }

  @Post('model')
  index(@Body() body: any): Observable<any> {
    return this.httpService.post(`api/v1/Model`, body)
  }

  @Post('fieldDefinition')
  postFieldDefintion(@Body() body: any): Observable<any> {
    return this.httpService.post(`api/v1/FieldDefinition`, body)
  }
}
