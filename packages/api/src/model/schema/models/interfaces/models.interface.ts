import { Document } from 'mongoose'

export interface Model extends Document {
  readonly name: string
}
