import { Catch, Injectable, NestMiddleware } from '@nestjs/common'
import { Request, Response } from 'express'

@Catch()
@Injectable()
export class SchemaMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: Function) {
    next()
  }
}
