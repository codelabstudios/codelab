import { g, NodeAttribute } from 'src/graph/Graph-variables'
import { BaseType, ValueFn } from 'd3-selection'
import { NodeType } from 'src/graph/Graph.interface'

type GetNodeAttribute<GElement extends BaseType, Datum> = (
  attr: keyof NodeAttribute,
) => ValueFn<GElement, Datum, string | number | boolean | null>

export const nodeAttribute: GetNodeAttribute<any, any> = (attr) => (d) => {
  const typename = d?.type?.name || NodeType.Default
  return g.node[typename][attr]
}
