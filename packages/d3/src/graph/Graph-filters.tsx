import { g } from 'src/graph/Graph-variables'

/**
 * Filters
 */

type GraphFilter<T> = (element: T) => boolean

export const activeNodes: GraphFilter<any> = (node) =>
  node.id === g.activeNode.id
export const nonActiveNodes: GraphFilter<any> = (node) =>
  node.id !== g.activeNode.id
export const nonActiveLinks: GraphFilter<any> = (link) =>
  link.id !== g.activeLink.id
