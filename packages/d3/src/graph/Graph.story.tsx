import React from 'react'
import { D3Graph } from 'src/graph/Graph'
import { d3GraphData } from 'src/graph/D3-graph.data'

export default {
  component: D3Graph,
  title: 'D3Graph',
}

export const basic = () => {
  return <D3Graph {...d3GraphData} />
}
