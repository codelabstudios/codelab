import React from 'react'
import { div, renderPropsData } from 'src/props/Props-renderProps.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Props',
}

export const Default = () => {
  const Component = TreeDom.render(renderPropsData)

  return <Component />
}

export const Children = () => {
  const Div = TreeDom.render(div)

  return <Div>Content</Div>
}
