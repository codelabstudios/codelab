import { InputNode } from '@codelab/graph'

export const renderPropsData: InputNode = {
  type: 'Html.div',
  props: {
    visibility: '',
    parentprops: {
      renderProps: true,
      value: {},
    },
  },
  children: [
    {
      type: 'Html.div',
      props: {
        childprops: {},
      },
    },
  ],
}

export const div = {
  type: 'Html.div',
}
