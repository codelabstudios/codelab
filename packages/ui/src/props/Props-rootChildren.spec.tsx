import React from 'react'
import { TreeDom } from 'src/renderer/TreeDom'

describe('RootChildren', () => {
  const data = {
    type: 'Provider',
    children: [
      {
        type: 'Button',
        children: [
          {
            type: 'Text',
            props: {
              value: 'Toggle Modal',
            },
          },
        ],
      },
      {
        type: 'Html.div',
        props: {
          'data-testid': 'div',
        },
      },
      {
        type: 'Modal',
        props: {
          title: 'Basic Modal',
          visible: true,
        },
      },
    ],
  }

  it('renders children to all available branches', () => {
    console.log(data)
    const Component = TreeDom.render(data)
    // render(
    //   <Component>
    //     <h1>Title</h1>
    //   </Component>,
    // )
    //
    // const modal = getByRole(document.body, 'dialog')
    // const div = getByTestId(document.body, 'div')
    //
    // const matcher: Matcher = (value, html) => {
    //   return value === 'Title' && html.nodeName === 'H1'
    // }
    //
    // const modalTitle = getByText(modal, matcher)
    // const divTitle = getByText(div, matcher)
    //
    // expect(modalTitle).toBeTruthy()
    // expect(divTitle).toBeTruthy()
  })

  /**
   * <A>
   *   <B>
   *     <C/>
   *   </B>
   * </A>
   *
   * This should put C in all valid children of B, which then puts that in all valid children of A
   */

  // it('renders nested children', () => {
  //
  // })
})
