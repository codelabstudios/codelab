import React from 'react'
import { sliderData } from 'src/components/slider/Slider.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Slider',
}

export const slider = () => {
  const Slider = TreeDom.render(sliderData)

  return <Slider />
}
