import { InputNode } from '@codelab/graph'
import { SliderProps } from 'src/components/slider/Slider'

export const sliderData: InputNode<SliderProps> = {
  type: 'Slider',
  props: {
    defaultValue: 20,
    min: 0,
    max: 50,
  },
}
