import React from 'react'
import { TreeDom } from 'src/renderer/TreeDom'
import { data, modalData } from 'src/components/modal/Modal.data'

export default {
  title: 'Modal',
}

export const Default = () => {
  const Modal = TreeDom.render(modalData)

  return <Modal />
}

export const WithChildren = () => {
  const Modal = TreeDom.render(data)

  return (
    <Modal>
      <div>working!</div>
    </Modal>
  )
}
