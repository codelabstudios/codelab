import { InputNode } from '@codelab/graph'

export const modalData: InputNode = {
  type: 'Provider',
  props: {
    ctx: {
      eval: true,
      value:
        'const [visible, setVisible] = this.React.useState(false); return { visible, setVisible }',
    },
    onOk: {
      eval: true,
      value: 'return () => this.setVisible(false)',
    },
    onCancel: {
      eval: true,
      value: 'return () => this.setVisible(false)',
    },
    visible: {
      eval: true,
      value: 'return this.visible',
    },
    onClick: {
      eval: true,
      value: 'return () => this.setVisible(true)',
    },
  },
  children: [
    {
      type: 'Button',
      children: [
        {
          type: 'Text',
          props: {
            value: 'Toggle Modal',
          },
        },
      ],
    },
    {
      type: 'Modal',
      props: {
        title: 'Basic Modal',
      },
      children: [
        {
          type: 'Html.p',
          children: [
            {
              type: 'Text',
              props: {
                value: 'Some contents...',
              },
            },
          ],
        },
      ],
    },
  ],
}

export const data: InputNode = {
  type: 'Provider',
  props: {
    ctx: {
      eval: true,
      value:
        'const [visible, setVisible] = this.React.useState(false); return { visible, setVisible }',
    },
    onOk: {
      eval: true,
      value: 'return () => this.setVisible(false)',
    },
    onCancel: {
      eval: true,
      value: 'return () => this.setVisible(false)',
    },
    visible: {
      eval: true,
      value: 'return this.visible',
    },
    onClick: {
      eval: true,
      value: 'return () => this.setVisible(true)',
    },
  },
  children: [
    {
      type: 'Button',
      children: [
        {
          type: 'Text',
          props: {
            value: 'Toggle Modal',
          },
        },
      ],
    },
    {
      type: 'Modal',
      props: {
        title: 'Basic Modal',
      },
    },
  ],
}
