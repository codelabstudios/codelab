import { Tabs as AntTabs } from 'antd'
import {
  TabPaneProps as AntTabPaneProps,
  TabsProps as AntTabsProps,
} from 'antd/lib/tabs'
import React, { PropsWithChildren } from 'react'

export type TabsProps = AntTabsProps
export type TabPaneProps = AntTabPaneProps

export namespace Tabs {
  export const Default: React.FC<PropsWithChildren<TabsProps>> = ({
    children,
    ...props
  }) => {
    return <AntTabs {...props}>{children}</AntTabs>
  }

  export const TabPane: React.FC<PropsWithChildren<TabPaneProps>> = ({
    children,
    ...props
  }) => {
    return <AntTabs.TabPane {...props}>{children}</AntTabs.TabPane>
  }
}
