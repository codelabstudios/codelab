import React from 'react'
import { tabsData } from 'src/components/tabs/Tabs.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Tabs',
}

export const Default = () => {
  const Tabs = TreeDom.render(tabsData)

  return <Tabs />
}
