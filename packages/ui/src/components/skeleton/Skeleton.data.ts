import { InputNode } from '@codelab/graph'

export const skeletonPropKeys = [
  'active',
  'avatar',
  'loading',
  'paragraph',
  'title',
] as const

export const skeletonData: InputNode = {
  type: 'Skeleton',
  props: {
    active: true,
    avatar: false,
    paragraph: true,
    title: true,
  },
}
