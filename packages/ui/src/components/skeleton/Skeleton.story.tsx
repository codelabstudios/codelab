import React from 'react'
import { skeletonData } from 'src/components/skeleton/Skeleton.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Skeleton',
}

export const Default = () => {
  const Skeleton = TreeDom.render(skeletonData)

  return <Skeleton />
}
