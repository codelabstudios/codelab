import React from 'react'
import { resultData } from 'src/components/result/Result.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Result',
}

export const Default = () => {
  const Result = TreeDom.render(resultData)

  return <Result />
}
