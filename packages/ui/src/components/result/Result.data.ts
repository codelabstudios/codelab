import { InputNode } from '@codelab/graph'

export const resultPropKeys = [
  'title',
  'subTitle',
  'status',
  'icon',
  'extra',
] as const

export const resultData: InputNode = {
  type: 'Result',
  props: {
    status: 'info',
  },
}
