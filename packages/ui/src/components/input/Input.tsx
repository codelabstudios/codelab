import React from 'react'
import { Input as AntInput } from 'antd'
import { InputProps as AntInputProps } from 'antd/lib/input/Input'
import { TextAreaProps as AntTextAreaProps } from 'antd/lib/input/TextArea'
import { PasswordProps as AntPasswordProps } from 'antd/lib/input/Password'
import { SearchProps as AntSearchProps } from 'antd/lib/input/Search'
import { GroupProps as AntGroupProps } from 'antd/lib/input/Group'

export type InputProps = AntInputProps
export type TextAreaProps = AntTextAreaProps
export type PasswordProps = AntPasswordProps
export type SearchProps = AntSearchProps
export type GroupProps = AntGroupProps

export namespace Input {
  export const Default: React.FC<InputProps> = (props) => (
    <AntInput {...props} />
  )

  export const TextArea: React.FC<TextAreaProps> = (props) => (
    <AntInput.TextArea {...props} />
  )

  export const Password: React.FC<PasswordProps> = (props) => (
    <AntInput.Password {...props} />
  )

  export const Search: React.FC<SearchProps> = (props) => (
    <AntInput.Search {...props} />
  )

  export const Group: React.FC<GroupProps> = (props) => (
    <AntInput.Group {...props} />
  )
}
