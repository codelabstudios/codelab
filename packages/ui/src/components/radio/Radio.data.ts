import { InputNode } from '@codelab/graph'

export const radioData: InputNode = {
  type: 'Radio.Group',
  props: {
    value: 'a',
    onChange: '',
  },
  children: [
    {
      type: 'Radio',
      props: {
        value: 'a',
      },
      children: [
        {
          type: 'Text',
          props: {
            value: 'A',
          },
        },
      ],
    },
    {
      type: 'Radio',
      props: {
        value: 'b',
      },
      children: [
        {
          type: 'Text',
          props: {
            value: 'B',
          },
        },
      ],
    },
    {
      type: 'Radio',
      props: {
        value: 'c',
      },
      children: [
        {
          type: 'Text',
          props: {
            value: 'C',
          },
        },
      ],
    },
  ],
}
