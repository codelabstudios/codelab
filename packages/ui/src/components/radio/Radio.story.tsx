import React from 'react'
import { radioData } from 'src/components/radio/Radio.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Radio',
}

export const Default = () => {
  const Radio = TreeDom.render(radioData)

  return <Radio />
}
