import React from 'react'
import { tableData } from 'src/components/table/Table.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Table',
}

export const Default = () => {
  const Table = TreeDom.render(tableData)

  return <Table />
}
