import { InputNode } from '@codelab/graph'

export const switchData: InputNode = {
  type: 'Switch',
  props: {
    checkedChildren: 'On',
    unCheckedChildren: 'Off',
  },
}
