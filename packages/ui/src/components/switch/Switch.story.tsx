import React from 'react'
import { switchData } from 'src/components/switch/Switch.data'
import { Switch as AntSwitch } from 'antd'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Switch',
}

export const base = () => {
  const Switch = TreeDom.render(switchData)

  return <Switch />
}

export const antd = () => {
  return (
    <AntSwitch checkedChildren="On" unCheckedChildren="Off" defaultChecked />
  )
}
