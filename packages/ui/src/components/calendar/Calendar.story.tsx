import React from 'react'
import { calendarData } from 'src/components/calendar/Calendar.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Calendar',
}

export const Default = () => {
  const Calendar = TreeDom.render(calendarData)

  return <Calendar />
}
