import { InputNode } from '@codelab/graph'

export const calendarPropKeys = [
  'dateCellRender',
  'dateFullCellRender',
  'defaultValue',
  'disabledDate',
  'fullscreen',
  'locale',
  'mode',
  'monthCellRender',
  'monthFullCellRender',
  'validRange',
  'value',
  'onPanelChange',
  'onSelect',
  'onChange',
  'headerRender',
]

export const calendarData: InputNode = {
  type: 'Calendar',
  props: {
    fullscreen: true,
    mode: 'month',
  },
}
