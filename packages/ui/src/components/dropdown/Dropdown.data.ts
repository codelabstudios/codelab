import { InputNode } from '@codelab/graph'

export const dropdownData: InputNode = {
  type: 'Dropdown',
  props: {
    overlay: {
      type: 'Menu',
      props: {
        mode: 'inline',
        style: {
          width: 320,
        },
      },
      children: [
        {
          type: 'Menu.Item',
          props: {
            key: '1',
            icon: 'UserOutlined',
          },
          children: [
            {
              type: 'Text',
              props: {
                value: 'Option 1',
              },
            },
          ],
        },
        {
          type: 'Menu.Item',
          props: {
            key: '2',
            icon: 'UserOutlined',
          },
          children: [
            {
              type: 'Text',
              props: {
                value: 'Option 2',
              },
            },
          ],
        },
      ],
    },
  },
  children: [
    {
      type: 'Html.a',
      props: {
        className: 'ant-dropdown-link',
      },
      children: [
        {
          type: 'Text',
          props: {
            value: 'Hover me',
          },
        },
      ],
    },
  ],
}
