import React from 'react'
import { dropdownData } from 'src/components/dropdown/Dropdown.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Dropdown',
}

export const base = () => {
  const Dropdown = TreeDom.render(dropdownData)

  return <Dropdown />
}
