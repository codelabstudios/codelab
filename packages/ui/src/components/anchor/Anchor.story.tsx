import React from 'react'
import { anchorData } from 'src/components/anchor/Anchor.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Anchor',
}

export const Default = () => {
  const Anchor = TreeDom.render(anchorData)

  return <Anchor />
}
