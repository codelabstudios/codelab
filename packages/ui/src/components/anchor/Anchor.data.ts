import { InputNode } from '@codelab/graph'

export const anchorPropKeys = [
  'affix',
  'bounds',
  'getContainer',
  'offsetTop',
  'showInkInFixed',
  'onClick',
  'getCurrentAnchor',
  'targetOffset',
  'onChange',
] as const

export const anchorData: InputNode = {
  type: 'Anchor',
  props: {
    affix: true,
    bounds: 5,
    offsetTop: 0,
    showInkInFixed: false,
  },
}
