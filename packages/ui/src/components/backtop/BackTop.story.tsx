import React from 'react'
import { backtopData } from 'src/components/backtop/BackTop.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'BackTop',
}

export const Default = () => {
  const BackTop = TreeDom.render(backtopData)

  return <BackTop />
}
