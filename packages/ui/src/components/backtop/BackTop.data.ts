import { InputNode } from '@codelab/graph'

export const backtopPropKeys = [
  'target',
  'visibilityHeight',
  'onClick',
] as const

export const backtopData: InputNode = {
  type: 'BackTop',
  props: {
    visibilityHeight: 400,
  },
}
