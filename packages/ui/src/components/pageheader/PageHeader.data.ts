import { InputNode } from '@codelab/graph'
import { PageHeaderProps } from 'antd/lib/page-header'

export const pageHeaderPropKeys: Array<keyof PageHeaderProps> = [
  'title',
  'subTitle',
  'ghost',
  'avatar',
  'backIcon',
  'tags',
  'extra',
  'breadcrumb',
  'footer',
  'onBack',
]

export const pageHeaderData: InputNode<PageHeaderProps> = {
  type: 'PageHeader',
  props: {
    title: 'abc',
    subTitle: 'xyz',
  },
}
