import React from 'react'
import { pageHeaderData } from 'src/components/pageheader/PageHeader.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'PageHeader',
}

export const Default = () => {
  const PageHeader = TreeDom.render(pageHeaderData)

  return <PageHeader />
}
