import React from 'react'
import { tooltipData } from 'src/components/tooltip/Tooltip.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Tooltip',
}

export const Default = () => {
  const Tooltip = TreeDom.render(tooltipData)

  return <Tooltip />
}
