import { InputNode } from '@codelab/graph'
import { TooltipProps } from 'antd/lib/tooltip'

export const tooltipPropKeys: Array<keyof TooltipProps> = [
  'title',
  'arrowPointAtCenter',
  'autoAdjustOverflow',
  'defaultVisible',
  'getPopupContainer',
  'mouseEnterDelay',
  'mouseLeaveDelay',
  'overlayClassName',
  'overlayStyle',
  'placement',
  'trigger',
  'visible',
  'onVisibleChange',
  'align',
  'destroyTooltipOnHide',
]

export const tooltipData: InputNode<TooltipProps> = {
  type: 'Tooltip',
  props: {
    title: 'prompt text',
    arrowPointAtCenter: false,
    autoAdjustOverflow: true,
    defaultVisible: false,
    mouseEnterDelay: 0.1,
    mouseLeaveDelay: 0.1,
    placement: 'top',
    trigger: 'hover',
    visible: false,
    destroyTooltipOnHide: false,
  },
}
