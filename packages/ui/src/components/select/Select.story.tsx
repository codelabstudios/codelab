import React from 'react'
import { selectData } from 'src/components/select/Select.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Select',
}

export const select = () => {
  const Select = TreeDom.render(selectData)

  return <Select />
}
