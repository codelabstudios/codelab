import React from 'react'
import { Select as AntSelect } from 'antd'
import { SelectProps as AntSelectProps, SelectValue } from 'antd/lib/select'
import { OptionProps as RcOptionProps } from 'rc-select/lib/Option'

export type SelectProps = AntSelectProps<SelectValue>
export type OptionsProps = RcOptionProps

export namespace Select {
  export const Default: React.FC<SelectProps> = ({ children, ...props }) => {
    return <AntSelect {...props}>{children}</AntSelect>
  }

  export const Option: React.FC<OptionsProps> = ({ children, ...props }) => {
    return <AntSelect.Option {...props}>{children}</AntSelect.Option>
  }
}
