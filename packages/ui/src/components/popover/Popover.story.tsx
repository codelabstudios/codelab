import React from 'react'
import { popoverData } from 'src/components/popover/Popover.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Popover',
}

export const Default = () => {
  const Popover = TreeDom.render(popoverData)

  return <Popover />
}
