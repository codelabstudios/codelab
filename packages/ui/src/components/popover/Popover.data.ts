import { InputNode } from '@codelab/graph'

export const popoverPropKeys = ['content', 'title'] as const

export const popoverData: InputNode = {
  type: 'Popover',
  props: {
    title: 'Title',
    content: 'Content',
  },
}
