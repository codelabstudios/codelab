import React from 'react'
import { Alert as AntAlert } from 'antd'
import { AlertProps } from 'antd/lib/alert'

export namespace Alert {
  export const Default = (props: AlertProps) => {
    return <AntAlert {...props} />
  }
}
