import { InputNode } from '@codelab/graph'
import { AlertProps } from 'antd/lib/alert'

export const alertData: InputNode<AlertProps> = {
  type: 'Alert',
  props: {
    message: 'Success Text',
    description:
      'Detailed description and advice about successful copywriting.',
    type: 'success',
    showIcon: true,
  },
}
