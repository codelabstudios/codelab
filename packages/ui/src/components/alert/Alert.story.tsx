import React from 'react'
import { alertData } from 'src/components/alert/Alert.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Alert',
}

export const Default = () => {
  const Alert = TreeDom.render(alertData)

  return <Alert />
}
