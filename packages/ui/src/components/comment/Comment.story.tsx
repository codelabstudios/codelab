import React from 'react'
import { commentData } from 'src/components/comment/Comment.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Comment',
}

export const Default = () => {
  const Comment = TreeDom.render(commentData)

  return <Comment />
}
