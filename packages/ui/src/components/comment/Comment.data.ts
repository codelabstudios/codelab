import { InputNode } from '@codelab/graph'
import { CommentProps } from 'antd/lib/comment'

export const commentPropKeys: Array<keyof CommentProps> = [
  'actions',
  'author',
  'avatar',
  'children',
  'content',
  'datetime',
]

export const commentData: InputNode<CommentProps> = {
  type: 'Comment',
  props: {
    author: 'Han Solo',
    content:
      'We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), to help people create their product prototypes beautifully and efficiently.',
    datetime: '1 July 2020',
  },
}
