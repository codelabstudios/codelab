import React from 'react'
import { uploadData } from 'src/components/upload/Upload.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Upload',
}

export const Default = () => {
  const Upload = TreeDom.render(uploadData)

  return <Upload />
}
