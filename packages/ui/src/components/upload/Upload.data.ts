import { InputNode } from '@codelab/graph'

export const uploadPropKeys = [
  'accept',
  'action',
  'method',
  'directory',
  'beforeUpload',
  'customRequest',
  'data',
  'defaultFileList',
  'disabled',
  'fileList',
  'headers',
  'listType',
  'multiple',
  'name',
  'previewFile',
  'isImageUrl',
  'showUploadList',
  'withCredentials',
  'openFileDialogOnClick',
  'onChange',
  'onPreview',
  'onRemove',
  'onDownload',
  'transformFile',
  'iconRender',
] as const

export const uploadData: InputNode = {
  type: 'Upload',
  props: {
    name: 'file',
    action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
    headers: {
      authorization: 'authorization-text',
    },
  },
}

// directory: false,
// disabled: false,
// multiple: true,
// showUploadList: true,
// withCredentials: false,
// openFileDialogOnClick: true,
