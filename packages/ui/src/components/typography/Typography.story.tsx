import React from 'react'
import { typographyData } from 'src/components/typography/Typography.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Typography',
}

export const Default = () => {
  const Typography = TreeDom.render(typographyData)
  return <Typography />
}
