import { InputNode } from '@codelab/graph'
import { ButtonProps } from 'src/components/button/Button'
import { TextProps } from 'src/components/text/Text'

export const buttonData: InputNode<ButtonProps | TextProps> = {
  type: 'Button',
  props: {
    type: 'primary',
  },
  children: [
    {
      type: 'Text',
      props: {
        value: 'Primary',
      },
    },
  ],
}
