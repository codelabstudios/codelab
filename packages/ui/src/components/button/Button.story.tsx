import React from 'react'
import { buttonData } from 'src/components/button/Button.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Button',
}

export const Default = () => {
  const Button = TreeDom.render(buttonData)

  return <Button />
}
