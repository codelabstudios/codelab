import React from 'react'
import { stepsData } from 'src/components/steps/Steps.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Steps',
}

export const Default = () => {
  const Steps = TreeDom.render(stepsData)

  return <Steps />
}
