import { InputNode } from '@codelab/graph'
// import { StepsProps, StepProps } from 'antd/lib/steps'

// export const stepsPropKeys: Array<keyof StepsProps> = [
//   'className',
//   'type',
//   'current',
//   'direction',
//   'labelPlacement',
//   'progressDot',
//   'size',
//   'status',
//   'initial',
//   'onChange',
// ]

// export const stepPropKeys: Array<keyof StepProps> = [
//   'description',
//   'icon',
//   'status',
//   'title',
//   'subTitle',
//   'disabled',
// ]

export const stepsData: InputNode = {
  type: 'Steps',
  props: {
    type: 'default',
    current: 10,
    direction: 'horizontal',
    labelPlacement: 'horizontal',
    progressDot: false,
    size: 'default',
    status: 'process',
    initial: 2,
  },
  // children:[
  //   {
  //     type: 'Steps.Step',
  //     props: {
  //       title: 'avfs',
  //       // subTitle: 'gjgug',
  //       // disabled:false
  //     },
  // ],
}
