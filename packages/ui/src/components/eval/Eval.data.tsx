import { InputNode } from '@codelab/graph'

export const onClickAlert: InputNode = {
  type: 'Button',
  props: {
    type: 'primary',
    onClick: {
      type: 'eval',
      value: 'return () => alert("Clicked!")',
    },
  },
  children: [
    {
      type: 'Text',
      props: {
        value: 'Alert',
      },
    },
  ],
}

export const toggleValue: InputNode = {
  type: 'Provider',
  props: {
    ctx: {
      type: 'eval',
      value:
        'const [truthy, toggleTruthy] = this.React.useState(true); return { truthy, toggleTruthy }',
    },
  },
  children: [
    {
      type: 'Button',
      props: {
        type: 'primary',
        onClick: {
          type: 'eval',
          value: 'return () => console.log(this.ctx.truthy)',
        },
      },
      children: [
        {
          type: 'Text',
          props: {
            value: 'Toggle',
          },
        },
      ],
    },
    {
      type: 'Html.div',
      children: [
        {
          type: 'Text',
          props: {
            value: 'Test',
          },
        },
      ],
    },
  ],
}
