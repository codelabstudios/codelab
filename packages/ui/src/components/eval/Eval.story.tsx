import React from 'react'
import { TreeDom } from 'src/renderer/TreeDom'
import { onClickAlert, toggleValue } from 'src/components/eval/Eval.data'

export default {
  title: 'Eval',
}

export const onClick = () => {
  const Button = TreeDom.render(onClickAlert)

  return <Button />
}

export const onClickToggle = () => {
  const Button = TreeDom.render(toggleValue)

  return <Button />
}
