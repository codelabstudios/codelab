import React from 'react'
import { messageData } from 'src/components/message/Message.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Message',
}

export const Default = () => {
  const Message = TreeDom.render(messageData)

  return <Message />
}
