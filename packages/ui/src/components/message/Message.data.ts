import { InputNode } from '@codelab/graph'

export const messagePropKeys = ['content', 'duration', 'onClose'] as const

export const messageData: InputNode = {
  type: 'Message',
  props: {
    content: 'message text',
    duration: 1.5,
  },
}
