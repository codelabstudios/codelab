import React from 'react'
import { TreeDom } from 'src/renderer/TreeDom'
import { divData } from 'src/components/html/Html.data'

export default {
  title: 'Html',
}

export const div = () => {
  const Div = TreeDom.render(divData)

  return <Div />
}
