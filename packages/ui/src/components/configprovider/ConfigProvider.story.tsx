import React from 'react'
import { configproviderData } from 'src/components/configprovider/ConfigProvider.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'ConfigProvider',
}

export const Default = () => {
  const ConfigProvider = TreeDom.render(configproviderData)

  return <ConfigProvider />
}
