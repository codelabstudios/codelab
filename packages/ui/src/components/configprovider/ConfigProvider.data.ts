import { InputNode } from '@codelab/graph'

export const configproviderPropKeys = [
  'autoInsertSpaceInButton',
  'componentSize',
  'csp',
  'form',
  'input',
  'renderEmpty',
  'getPopupContainer',
  'getTargetContainer',
  'locale',
  'prefixCls',
  'pageHeader',
  'direction',
  'space',
] as const

export const configproviderData: InputNode = {
  type: 'ConfigProvider',
  props: {
    autoInsertSpaceInButton: true,
    prefixCls: 'ant',
    pageHeader: 'true',
    direction: 'ltr',
  },
}
