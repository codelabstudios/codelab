import React from 'react'
import { avatarData } from 'src/components/avatar/Avatar.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Avatar',
}

export const Default = () => {
  const Avatar = TreeDom.render(avatarData)

  return <Avatar />
}
