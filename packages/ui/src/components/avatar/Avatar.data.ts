import { InputNode } from '@codelab/graph'

export const avatarPropKeys = [
  'icon',
  'shape',
  'size',
  'src',
  'srcSet',
  'alt',
  'onError',
  'gap',
]

export const avatarData: InputNode = {
  type: 'Avatar',
  props: {
    shape: 'circle',
    size: 'default',
    gap: 4,
  },
}
