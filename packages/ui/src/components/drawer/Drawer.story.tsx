import React from 'react'
import { drawerData } from 'src/components/drawer/Drawer.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Drawer',
}

export const Default = () => {
  const Drawer = TreeDom.render(drawerData)

  return <Drawer />
}
