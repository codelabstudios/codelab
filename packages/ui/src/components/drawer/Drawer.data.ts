import { InputNode } from '@codelab/graph'

export const drawerPropKeys = [
  'closable',
  'destroyOnClose',
  'forceRender',
  'getContainer',
  'mask',
  'maskClosable',
  'maskStyle',
  'style',
  'drawerStyle',
  'headerStyle',
  'bodyStyle',
  'title',
  'visible',
  'width',
  'height',
  'className',
  'zIndex',
  'placement',
  'onClose',
  'afterVisibleChange',
  'keyboard',
  'footer',
  'footerStyle',
] as const

export const drawerData: InputNode = {
  type: 'Drawer',
  props: {
    closable: true,
    destroyOnClose: false,
    forceRender: false,
    mask: true,
    maskClosable: true,
    visible: false,
    width: 256,
    height: 256,
    zIndex: 1000,
    placement: 'right',
    keyboard: true,
  },
}
