import React from 'react'
import { spinData } from 'src/components/spin/Spin.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Spin',
}

export const Default = () => {
  const Spin = TreeDom.render(spinData)

  return <Spin />
}
