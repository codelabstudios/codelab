import { InputNode } from '@codelab/graph'

export const spinPropKeys = [
  'delay',
  'indicator',
  'size',
  'spinning',
  'tip',
  'wrapperClassName',
] as const

export const spinData: InputNode = {
  type: 'Spin',
  props: {
    size: 'default',
    spinning: true,
  },
}
