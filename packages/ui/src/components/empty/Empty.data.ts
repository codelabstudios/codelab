import { InputNode } from '@codelab/graph'
import { EmptyProps } from 'antd/lib/empty'

export const emptyPropKeys: Array<keyof EmptyProps> = [
  'description',
  'imageStyle',
  'image',
]

export const emptyData: InputNode<EmptyProps> = {
  type: 'Empty',
  props: {
    description: 'No Data',
  },
}
