import React from 'react'
import { emptyData } from 'src/components/empty/Empty.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Empty',
}

export const Default = () => {
  const Empty = TreeDom.render(emptyData)

  return <Empty />
}
