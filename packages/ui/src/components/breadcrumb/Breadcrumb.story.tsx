import React from 'react'
import { breadcrumbData } from 'src/components/breadcrumb/Breadcrumb.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Breadcrumb',
}

export const Default = () => {
  const Breadcrumb = TreeDom.render(breadcrumbData)

  return <Breadcrumb />
}
