import React from 'react'
import { formData } from 'src/components/form/Form.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Form',
}

export const Default = () => {
  const Form = TreeDom.render(formData)

  return <Form />
}
