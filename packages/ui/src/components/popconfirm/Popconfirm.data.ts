import { InputNode } from '@codelab/graph'

export const popconfirmPropKeys = [
  'cancelText',
  'okText',
  'okType',
  'okButtonProps',
  'cancelButtonProps',
  'title',
  'onCancel',
  'onConfirm',
  'icon',
  'disabled',
] as const

export const popconfirmData: InputNode = {
  type: 'Popconfirm',
  props: {
    cancelText: 'Cancel',
    okText: 'OK',
    okType: 'primary',
    disabled: false,
  },
}
