import React from 'react'
import { popconfirmData } from 'src/components/popconfirm/Popconfirm.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Popconfirm',
}

export const Default = () => {
  const Popconfirm = TreeDom.render(popconfirmData)

  return <Popconfirm />
}
