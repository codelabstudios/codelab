import React, { useState } from 'react'
import { Layout as AntLayout } from 'antd'

const {
  Header: AntHeader,
  Footer: AntFooter,
  Sider: AntSider,
  Content: AntContent,
} = AntLayout

export interface LayoutProps {
  Header: React.ReactElement
  Sidebar?: React.ReactElement
  sidebarConfig?: {
    collapsible: boolean
    collapsed: boolean
    onCollapse(): any
  }
  Content: React.ReactElement
  Footer?: React.ReactElement
}

export const Layout = ({ Header, Sidebar, Content, Footer }: LayoutProps) => {
  const [collapsed, setCollapsed] = useState(false)

  return (
    <AntLayout style={{ minHeight: '100vh' }} hasSider>
      <AntSider
        onCollapse={() => setCollapsed(!collapsed)}
        collapsible
        collapsed={collapsed}
      >
        {Sidebar}
      </AntSider>
      <AntLayout>
        <AntHeader>{Header}</AntHeader>
        <AntContent>{Content}</AntContent>
        <AntFooter>{Footer}</AntFooter>
      </AntLayout>
    </AntLayout>
  )
}
