import { InputNode } from '@codelab/graph'

export const carouselPropKeys = [
  'afterChange',
  'autoplay',
  'beforeChange',
  'dotPosition',
  'dots',
  'easing',
  'effect',
] as const

export const carouselData: InputNode = {
  type: 'Carousel',
  props: {
    autoplay: false,
    dotPosition: 'bottom',
    dots: true,
    easing: 'linear',
    effect: 'scrollx',
  },
}
