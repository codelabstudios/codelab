import React from 'react'
import { carouselData } from 'src/components/carousel/Carousel.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Carousel',
}

export const Default = () => {
  const Carousel = TreeDom.render(carouselData)

  return <Carousel />
}
