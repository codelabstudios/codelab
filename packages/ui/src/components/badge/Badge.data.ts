import { InputNode } from '@codelab/graph'
import { BadgeProps } from 'antd/lib/badge'

export const badgePropKeys: Array<keyof BadgeProps> = [
  'color',
  'count',
  'dot',
  'offset',
  'overflowCount',
  'showZero',
  'status',
  'text',
  'title',
]

export const badgeData: InputNode<BadgeProps> = {
  type: 'Badge',
  props: {
    count: 5,
    dot: false,
    overflowCount: 99,
    showZero: false,
    color: '#f5222d',
    title: 'count',
  },
}
