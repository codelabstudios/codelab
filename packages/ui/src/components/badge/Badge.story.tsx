import React from 'react'
import { badgeData } from 'src/components/badge/Badge.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Badge',
}

export const Default = () => {
  const Badge = TreeDom.render(badgeData)

  return <Badge />
}
