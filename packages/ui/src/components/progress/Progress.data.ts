import { InputNode } from '@codelab/graph'

export const progressPropKeys = [
  'type',
  'format',
  'percent',
  'showInfo',
  'status',
  'strokeLinecap',
  'strokeColor',
  'trailColor',
] as const

export const progressData: InputNode = {
  type: 'Progress',
  props: {
    type: 'line',
    percent: 0,
    showInfo: true,
    strokeLinecap: 'round',
  },
}
