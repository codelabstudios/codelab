import React from 'react'
import { progressData } from 'src/components/progress/Progress.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Progress',
}

export const Default = () => {
  const Progress = TreeDom.render(progressData)

  return <Progress />
}
