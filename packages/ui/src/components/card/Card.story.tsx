import React from 'react'
import { cardData } from 'src/components/card/Card.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Card',
}

export const Default = () => {
  const Card = TreeDom.render(cardData)

  return <Card />
}
