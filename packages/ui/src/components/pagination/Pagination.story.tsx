import React from 'react'
import { paginationData } from 'src/components/pagination/Pagination.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Pagination',
}

export const Default = () => {
  const Pagination = TreeDom.render(paginationData)

  return <Pagination />
}
