import { InputNode } from '@codelab/graph'
import { PaginationProps } from 'antd/lib/pagination'

export const paginationPropKeys: Array<keyof PaginationProps> = [
  'current',
  'defaultCurrent',
  'defaultPageSize',
  'disabled',
  'hideOnSinglePage',
  'itemRender',
  'pageSize',
  'pageSizeOptions',
  'showLessItems',
  'showQuickJumper',
  'showSizeChanger',
  'showTitle',
  'showTotal',
  'simple',
  'size',
  'responsive',
  'total',
  'onChange',
  'onShowSizeChange',
]

export const paginationData: InputNode<PaginationProps> = {
  type: 'Pagination',
  props: {
    defaultCurrent: 1,
    defaultPageSize: 10,
    hideOnSinglePage: false,
    pageSizeOptions: ['10', '20', '50', '100'],
    showLessItems: false,
    showQuickJumper: false,
    showTitle: true,
    total: 100,
  },
}
