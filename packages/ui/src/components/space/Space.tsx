import React from 'react'
import { Space as AntSpace } from 'antd'
import { SpaceProps } from 'antd/lib/space'

export namespace Space {
  export const Default: React.FC<SpaceProps> = ({ children, ...props }) => {
    return <AntSpace {...props}>{children}</AntSpace>
  }
}
