import { InputNode } from '@codelab/graph'

export const checkboxData: InputNode = {
  type: 'Checkbox',
  children: [
    {
      type: 'Text',
      props: {
        value: 'Checkbox',
      },
    },
  ],
}
