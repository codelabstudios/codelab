import React from 'react'
import { checkboxData } from 'src/components/checkbox/Checkbox.data'
import { TreeDom } from 'src/renderer/TreeDom'

export default {
  title: 'Checkbox',
}

export const Default = () => {
  const Checkbox = TreeDom.render(checkboxData)

  return <Checkbox />
}
