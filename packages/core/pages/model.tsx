import {
  Field,
  fieldSchemaDefinition,
  mongooseRecordsToDataSource,
  schemaTypeToTableColumns,
} from '@codelab/schema'
import { TreeDom } from '@codelab/ui'
import { GetStaticProps, InferGetStaticPropsType } from 'next'
import React from 'react'
import { createModelForm, modalData } from 'pages/model-data'

export const getStaticProps: GetStaticProps = async () => {
  const res = await fetch('http://localhost:4000/api/v1/Field')
  const fields: Array<Field> = await res.json()

  return {
    props: {
      fields,
    },
  }
}

const ModelPage = ({
  fields,
}: InferGetStaticPropsType<typeof getStaticProps>) => {
  const columns = schemaTypeToTableColumns(fieldSchemaDefinition)
  const dataSource: any = mongooseRecordsToDataSource(fields)

  const tableData = {
    type: 'Table',
    props: {
      dataSource,
      columns,
    },
  }
  const Table = TreeDom.render(tableData)
  const Modal = TreeDom.render(modalData as any)
  const CreateModelForm = TreeDom.render(createModelForm as any)

  return (
    <>
      <Modal>
        <CreateModelForm />
      </Modal>
      <Table />
    </>
  )
}

export default ModelPage
