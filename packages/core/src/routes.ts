/* eslint-disable @typescript-eslint/no-var-requires */
/* @ts-ignore */
const Routes = require('next-routes')

module.exports = new Routes()
  .add('index', '/', 'index')
  // .add('app', '/:username/:app/:page')
  // .add('graph', '/graph', 'graph')
  .add('model', '/model/:modelID', 'model')
// .add('content', '/content/:modelID', 'content')
