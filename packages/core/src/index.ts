/* eslint-disable @typescript-eslint/no-var-requires */
/* @ts-ignore */
require('reflect-metadata')
const express = require('express')
const next = require('next')
const routes = require('./routes')

const app = next({ dev: process.env.NODE_ENV !== 'prod' })
const handler = routes.getRequestHandler(app)

const bootstrap = async () => {
  app.prepare().then(() => {
    express().use(handler).listen(3000)
  })
}

bootstrap()
