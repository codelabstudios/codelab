import { Document, Schema } from 'mongoose'

export interface Field extends Document {
  readonly definition: Schema.Types.Mixed
}
