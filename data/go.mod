module gitlab.com/codelabstudios/codelab/data

go 1.14

require (
	github.com/gin-gonic/gin v1.6.1
	github.com/go-kit/kit v0.10.0
	github.com/namsral/flag v1.7.4-pre
)
