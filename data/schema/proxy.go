package main

import (
	"github.com/go-kit/kit/endpoint"
	httptransport "github.com/go-kit/kit/transport/http"
	"net/url"
	"strings"
)

type proxymw struct {
	// This is inheritance, must override
	next      GraphService     // Serve most requests via this service...
	addGraph endpoint.Endpoint // ...except Uppercase, which gets served by this endpoint
}

func (mw proxymw) AddGraph(string) (string, error) {
	panic("implement me")
}

func proxyingMiddleware(instance string) ServiceMiddleware {
	return func(next GraphService) GraphService {
		return proxymw{next, makeAddGraphProxy(instance)}
	}
}

func makeAddGraphProxy(instance string) endpoint.Endpoint {
	//Normalize to http
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}

	//Check for error
	proxyURL, err := url.Parse(instance)
	if err != nil {
		panic(err)
	}

	//Set default URI
	if proxyURL.Path == "" {
		proxyURL.Path = "/add-graph"
	}

	return httptransport.NewClient(
		"GET",
		proxyURL,
		encodeRequest,
		decodeAddGraphResponse,
	).Endpoint()
}
