package main

type GraphService interface {
	AddGraph(string) (string, error)
}

type graphService struct{}

func (graphService) AddGraph(s string) (string, error) {
	return s, nil
}

type ServiceMiddleware func(GraphService) GraphService
