package main

import (
	"fmt"
	httptransport "github.com/go-kit/kit/transport/http"
	"github.com/namsral/flag"
	"log"
	"net/http"
)

func main() {
	//parseFlags()

	svc := graphService{}

	addGraphHandler := httptransport.NewServer(
		makeAddGraphEndpoint(svc),
		decodeAddGraphRequest,
		encodeResponse,
	)

	http.Handle("/add-graph", addGraphHandler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func parseFlags() {
	var appPort int
	flag.IntVar(&appPort, "appPort", 8080, "port of app")
	flag.Parse()
	fmt.Print("appPort:", appPort)
}
