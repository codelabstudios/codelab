package main

import (
	"bytes"
	"context"
	"encoding/json"
	"github.com/go-kit/kit/endpoint"
	"io/ioutil"
	"net/http"
)

type addGraphRequest struct {
	Input string `json:"input"`
}

type addGraphResponse struct {
	Output string `json:"output"`
	Err    string `json:"err,omitempty"`
}

func makeAddGraphEndpoint(svc GraphService) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		req := request.(addGraphRequest)
		output, err := svc.AddGraph(req.Input)
		if err != nil {
			return addGraphResponse{output, err.Error()}, nil
		}
		return addGraphResponse{output, ""}, nil
	}
}

func decodeAddGraphRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request addGraphRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func encodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	return json.NewEncoder(w).Encode(response)
}

func encodeRequest(_ context.Context, r *http.Request, request interface{}) error {
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(request); err != nil {
		return err
	}
	r.Body = ioutil.NopCloser(&buf)
	return nil
}

func decodeAddGraphResponse(_ context.Context, r *http.Response) (interface{}, error) {
	var response addGraphResponse
	if err := json.NewDecoder(r.Body).Decode(&response); err != nil {
		return nil, err
	}
	return response, nil
}
