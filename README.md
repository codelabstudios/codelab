# Getting Started

1. Install MongoDB locally
2. Copy `env.example` to `dev.env`
3. Copy `.scripts.config.example.yml` to `.scripts.config.yml` & configure
4. `yarn`
5. `yarn build` (Only needs to be done for fresh clone, you may need to run this command several times due to package dependency & compile order)
6. `yarn dev` & visit `http://localhost:3000`
7. `yarn storybook` & visit `http://localhost:9000` (in new tab)
8. `yarn jest` (in new tab)
9. Visit [Codelab Documentation](https://docs.codelabstudios.com)
